/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGenerator;
import csg.CourseSiteGeneratorProp;
import csg.data.TeachingAssistant;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;

/**
 *
 * @author zhaotingyi
 */
public class CourseSiteGeneratorWorkspace implements Initializable{
    
//    private final CourseSiteGenerator app;
    
//    public CourseSiteGeneratorWorkspace(CourseSiteGenerator initapp){
//        app = initapp;
        
//    }
    
    @FXML private Menu menu_file;
    @FXML private Menu menu_edit;
    @FXML private Menu menu_help;
    @FXML private MenuItem menu_file_new;
    @FXML private MenuItem menu_file_save;
    @FXML private MenuItem menu_file_saveAs;
    @FXML private MenuItem menu_file_load;
    @FXML private MenuItem menu_file_export;
    @FXML private MenuItem menu_file_exit;
    @FXML private MenuItem menu_edit_undo;
    @FXML private MenuItem menu_edit_redo;
    @FXML private MenuItem menu_help_about;
    //
    @FXML private Tab tab_courseDetails;
    @FXML private Tab tab_taData;
    @FXML private Tab tab_recitationData;
    @FXML private Tab tab_scheduleData;
    @FXML private Tab tab_projectData;
    //
    //
    @FXML private Text TA_taTitle;
    @FXML private TableView<TeachingAssistant> TAtable;
    @FXML private TableColumn<TeachingAssistant, Boolean> TAundergradColumn;
    @FXML private TableColumn<TeachingAssistant, String> TAnameColumn; 
    @FXML private TableColumn<TeachingAssistant, String> TAemailColumn;
    @FXML private TextField TAnameinput;
    @FXML private TextField TAemailinput;
    @FXML private Button TAaddButton;
    @FXML private Button TAclearButton;
    @FXML private Text OfficeHourTitle;
    @FXML private Text OfficeHourStart;
    @FXML private Text OfficeHourEnd;
    @FXML private ComboBox OfficeHourStartBox;
    @FXML private ComboBox OfficeHourEndBox;
    @FXML private TableView<TeachingAssistant> OfficeHourTable;
    @FXML private TableColumn<TeachingAssistant, Boolean> OfficeHourStartColumn;
    @FXML private TableColumn<TeachingAssistant, String> OfficeHourEndColumn; 
    @FXML private TableColumn<TeachingAssistant, String> OfficeHourMonColumn; 
    @FXML private TableColumn<TeachingAssistant, String> OfficeHourTuesColumn; 
    @FXML private TableColumn<TeachingAssistant, String> OfficeHourWedColumn; 
    @FXML private TableColumn<TeachingAssistant, String> OfficeHourThusColumn; 
    @FXML private TableColumn<TeachingAssistant, String> OfficeHourFriColumn;
    //
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        menu_file.setText(props.getProperty(CourseSiteGeneratorProp.MENU_FILE_TEXT));
        menu_edit.setText(props.getProperty(CourseSiteGeneratorProp.MENU_EDIT_TEXT));
        menu_help.setText(props.getProperty(CourseSiteGeneratorProp.MENU_HELP_TEXT));
        menu_file_new.setText(props.getProperty(CourseSiteGeneratorProp.MENU_NEW_TEXT));
        menu_file_save.setText(props.getProperty(CourseSiteGeneratorProp.MENU_SAVE_TEXT));
        menu_file_saveAs.setText(props.getProperty(CourseSiteGeneratorProp.MENU_SAVEAS_TEXT));
        menu_file_load.setText(props.getProperty(CourseSiteGeneratorProp.MENU_LOAD_TEXT));
        menu_file_export.setText(props.getProperty(CourseSiteGeneratorProp.MENU_EXPORT_TEXT));
        menu_file_exit.setText(props.getProperty(CourseSiteGeneratorProp.MENU_EXIT_TEXT));
        menu_edit_undo.setText(props.getProperty(CourseSiteGeneratorProp.MENU_UNDO_TEXT));
        menu_edit_redo.setText(props.getProperty(CourseSiteGeneratorProp.MENU_REDO_TEXT));
        menu_help_about.setText(props.getProperty(CourseSiteGeneratorProp.MENU_ABOUT_TEXT));
        
        tab_courseDetails.setText(props.getProperty(CourseSiteGeneratorProp.TAB_COURSEDETAILS_TEXT));
        tab_taData.setText(props.getProperty(CourseSiteGeneratorProp.TAB_TADATA_TEXT));
        tab_recitationData.setText(props.getProperty(CourseSiteGeneratorProp.TAB_RECITATIONDATA_TEXT));
        tab_scheduleData.setText(props.getProperty(CourseSiteGeneratorProp.TAB_SCHEDULEDATA_TEXT));
        tab_projectData.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
   
        OfficeHourTitle.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_TITLE_TEXT));
        OfficeHourStart.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_START_TEXT));
        OfficeHourEnd.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_END_TEXT));
        OfficeHourStartColumn.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_START_CLUM_TEXT));
        OfficeHourEndColumn.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_END_CLUM_TEXT));
        OfficeHourMonColumn.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_MON_CLUM_TEXT));
        OfficeHourTuesColumn.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_TUES_CLUM_TEXT));
        OfficeHourWedColumn.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_WED_CLUM_TEXT));
        OfficeHourThusColumn.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_THUS_CLUM_TEXT));
        OfficeHourFriColumn.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_FRI_CLUM_TEXT));
        
        TA_taTitle.setText(props.getProperty(CourseSiteGeneratorProp.TA_TITLE_TEXT));
        TAundergradColumn.setText(props.getProperty(CourseSiteGeneratorProp.TA_UNDERGRAD_CLUM_TEXT));
        TAnameColumn.setText(props.getProperty(CourseSiteGeneratorProp.TA_NAME_CLUM_TEXT));
        TAemailColumn.setText(props.getProperty(CourseSiteGeneratorProp.TA_EMAIL_CLUM_TEXT));
        TAnameinput.setPromptText(props.getProperty(CourseSiteGeneratorProp.TA_NAME_INPUT_TEXT));
        TAemailinput.setPromptText(props.getProperty(CourseSiteGeneratorProp.TA_EMAIL_INPUT_TEXT));
        TAaddButton.setText(props.getProperty(CourseSiteGeneratorProp.TA_ADD_BUTTON_TEXT));
        TAclearButton.setText(props.getProperty(CourseSiteGeneratorProp.TA_CLEAR_BUTTON_TEXT));
        TAundergradColumn.setCellFactory( e -> new CheckBoxTableCell<>());
    } 
}
