/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGenerator;

/**
 *
 * @author zhaotingyi
 */
public class CourseSiteGeneratorData {
    private final CourseSiteGenerator app;
    private final CourseSiteGeneratorTAData TAData;
    
    public CourseSiteGeneratorData(CourseSiteGenerator initapp){
        app = initapp;
        TAData = new CourseSiteGeneratorTAData(initapp);
    }
}
