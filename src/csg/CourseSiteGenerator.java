/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg;

import csg.data.CourseSiteGeneratorData;
import csg.workspace.CourseSiteGeneratorWorkspace;
import java.util.Optional;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author zhaotingyi
 */
public class CourseSiteGenerator extends Application {
    private CourseSiteGeneratorWorkspace workspaceComponent;
    private CourseSiteGeneratorData dataComponent;
    
    
    @Override
    public void start(Stage stage) throws Exception {
        
	try {
	    // LOAD APP PROPERTIES, BOTH THE BASIC UI STUFF FOR THE FRAMEWORK
	    // AND THE CUSTOM UI STUFF FOR THE WORKSPACEAlert _alert = new Alert(Alert.AlertType.CONFIRMATION,p_message,new ButtonType("取消", ButtonBar.ButtonData.NO),
               Alert _alert = new Alert(Alert.AlertType.CONFIRMATION,"",new ButtonType("English", ButtonBar.ButtonData.NO),new ButtonType("Chinese", ButtonBar.ButtonData.YES));
                _alert.setTitle("Choose your language");
                _alert.setHeaderText("Choose your language.");
                Optional<ButtonType> _buttonType = _alert.showAndWait();
                boolean success;
                if(_buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)){
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    props.addProperty("DATA_PATH", "./data/");
                    props.loadProperties("app_properties_chinese.xml", "properties_schema.xsd");
                }
                else {
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    props.addProperty("DATA_PATH", "./data/");
                    props.loadProperties("app_properties_english.xml", "properties_schema.xsd");
                }
	}catch (Exception e) {
	}
        
//        workspaceComponent = new CourseSiteGeneratorWorkspace(this);
        dataComponent = new CourseSiteGeneratorData(this);
        
        Parent root = FXMLLoader.load(getClass().getResource("csg.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public CourseSiteGeneratorWorkspace getWorkspace(){
        return workspaceComponent;
    }
    
    public CourseSiteGeneratorData getData(){
        return dataComponent;
    }
    
}
